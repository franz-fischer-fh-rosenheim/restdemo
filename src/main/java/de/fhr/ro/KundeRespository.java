package de.fhr.ro;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by franz on 16.05.17.
 */
public interface KundeRespository extends CrudRepository<Kunde,Long>{

    public Iterable<Kunde> findByNachname(String nachname);
    public Iterable<Kunde> findByVorname(String vorname);
    public Iterable<Kunde> findByVornameAndNachname(String vorname, String nachname);
}
