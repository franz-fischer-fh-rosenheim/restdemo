package de.fhr.ro;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by franz on 16.05.17.
 */
@Entity
public class Kunde {
    @Id
    @GeneratedValue
    private Long identitaet;
    private String vorname;
    private String nachname;

    public Kunde(){}

    public Kunde(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kunde kunde = (Kunde) o;

        return identitaet.equals(kunde.identitaet);
    }

    @Override
    public int hashCode() {
        return identitaet.hashCode();
    }
}
