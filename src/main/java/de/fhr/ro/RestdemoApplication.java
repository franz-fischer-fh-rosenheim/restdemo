package de.fhr.ro;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RestdemoApplication {

	@Bean
	InitializingBean seedDatabase(KundeRespository repo){
		return new InitializingBean() {
			@Override
			public void afterPropertiesSet() throws Exception {
				repo.save(new Kunde("Harry","Hirsch"));
				repo.save(new Kunde("Willi","Winzig"));
				repo.save(new Kunde("Bernd","Winzig"));
				System.out.println("Datenbank seeded");
			}
		};
	}

	@Bean
	CommandLineRunner beispiel(KundeRespository repo){
		return new CommandLineRunner() {
			@Override
			public void run(String... args) throws Exception {
				repo.findByNachname("Winzig").forEach(System.out::println);
				System.out.println("Alle Winzigs waren das");
				repo.findByVornameAndNachname("Harry","Hirsch").forEach(System.out::println);
				System.out.println("DAS war Harry");
			}
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(RestdemoApplication.class, args);
	}
}
