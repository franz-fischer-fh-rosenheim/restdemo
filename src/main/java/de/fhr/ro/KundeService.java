package de.fhr.ro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by franz on 16.05.17.
 */

@RestController
public class KundeService {
    @Autowired
    private KundeRespository repo;

    @RequestMapping(path="/demo" ,method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    List<Kunde> getKundenlliste(){
        ArrayList<Kunde> result=new ArrayList<>();
        Iterable<Kunde> query = repo.findAll();

        for (Kunde k: query){
            result.add(k);
        }

        return result;

    }

    @RequestMapping(path="/demo/{id}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Kunde> findById(@PathVariable String id){
        Kunde k=repo.findOne(Long.parseLong(id));
        return new ResponseEntity<Kunde>(k, HttpStatus.I_AM_A_TEAPOT);
    }
}
